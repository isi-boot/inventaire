package tn.isiboot.Inventaire.domain;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class Client implements Serializable {
    private Long idClient;
    private String nomClient;
    private String prenomClient;
    private String adresseClient;
    private String telephoneClient;

}
