package tn.isiboot.Inventaire.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_COLIS", discriminatorType = DiscriminatorType.STRING, length = 2)

public abstract class Colis implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idColis;
    private String description;
    private Date dateColis;

    private Collection<Materiel> materiels;


    public Colis() {
    }

    public Long getIdColis() {
        return idColis;
    }

    public void setIdColis(Long idColis) {
        this.idColis = idColis;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
