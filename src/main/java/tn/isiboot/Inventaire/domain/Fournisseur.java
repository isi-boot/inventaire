package tn.isiboot.Inventaire.domain;

import javax.persistence.Entity;
import java.io.Serializable;

@Entity
public class Fournisseur implements Serializable {
    private Long idFournisseur;
    private String nomFournisseur;
    private String adresseFournisseur;
    private String telephoneFournisseur;
}
