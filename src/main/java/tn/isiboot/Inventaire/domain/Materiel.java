package tn.isiboot.Inventaire.domain;

import javax.persistence.Entity;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Collection;

@Entity
public class Materiel implements Serializable {
    private Long idMateriel;
    private String nomMateriel;
    private int quatity;

    @ManyToOne
    private Collection<Colis> colis;

}
